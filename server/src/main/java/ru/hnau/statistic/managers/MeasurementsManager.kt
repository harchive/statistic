package ru.hnau.statistic.managers

import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Example
import org.springframework.stereotype.Component
import ru.hnau.jutils.finisher.Finisher
import ru.hnau.statistic.data.entity.MeasurementDb
import ru.hnau.statistic.data.entity.MeasurementDbRepository
import ru.hnau.statistic.data.update.CategoriesUpdater
import ru.hnau.statistic_common.category.Category
import ru.hnau.statistic_common.country.Country
import ru.hnau.statistic_common.measurement.Measurement


@Component
class MeasurementsManager {

    private val logger = LogManager.getLogger(this::class.java)

    @Autowired
    lateinit var repository: MeasurementDbRepository

    fun getCategoryMeasurements(category: Category) =
            getLastMeasurements(repository.findByCategory(category))

    fun getCountryMeasurements(country: Country) =
            getLastMeasurements(repository.findByCountry(country))


    private fun getLastMeasurements(measurements: List<MeasurementDb>) =
            measurements
                    .groupBy { it.category to it.country }
                    .values
                    .mapNotNull {
                        it.maxBy { it.measured ?: it.received ?: 0L }
                    }

    fun update(categories: List<Category> = Category.values().toList()) {
        get(categories).await { measurements ->
            logger.debug("Received measurements: $measurements")
            saveAllNew(measurements)
        }
    }

    fun clear() = repository.deleteAll()

    private fun get(category: Category): Finisher<List<Measurement>> {

        val updater = CategoriesUpdater.getUpdater(category)
        if (updater == null) {
            logger.error("No updater found for category $category")
            return Finisher.forExistenceData(emptyList())
        }

        return updater.getMeasurements(category)
    }

    private fun get(categories: List<Category> = Category.values().toList()) =
            Finisher.flatten(categories.map { get(it) })

    private fun saveAllNew(measurements: List<Measurement>) {

        val newMeasurements = measurements.filter { measurement ->
            val lastMeasurementWithSameCountryAndCategory =
                    getLastMeasurement(measurement.country, measurement.category)
                            ?: return@filter true

            if (measurement.floatValue != lastMeasurementWithSameCountryAndCategory.floatValue) {
                return@filter true
            }

            if (measurement.longValue != lastMeasurementWithSameCountryAndCategory.longValue) {
                return@filter true
            }

            logger.debug("New measurement $measurement and last measurement $lastMeasurementWithSameCountryAndCategory has same values")
            return@filter false

        }.map { MeasurementDb(it) }

        repository.saveAll(newMeasurements)
    }

    private fun getLastMeasurement(country: Country, category: Category): MeasurementDb? {
        val measurement = MeasurementDb(category = category, country = country)
        val example = Example.of(measurement)
        return repository.findAll(example).maxBy { it.measured ?: it.received ?: 0L }
    }

}