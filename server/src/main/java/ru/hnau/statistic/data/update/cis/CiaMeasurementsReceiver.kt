package ru.hnau.statistic.data.update.cis

import org.apache.logging.log4j.LogManager
import ru.hnau.jutils.finisher.NewThreadFinisher
import ru.hnau.jutils.tryCatch
import ru.hnau.jutils.tryOrElse
import ru.hnau.statistic.data.update.CategoryMeasurementsReceiver
import ru.hnau.statistic.utils.SimpleLoader
import ru.hnau.statistic.utils.messageOrUndefined
import ru.hnau.statistic_common.category.Category
import ru.hnau.statistic_common.measurement.Measurement


object CiaMeasurementsReceiver : CategoryMeasurementsReceiver {

    private val EMPTY_RESULT = emptyList<Measurement>()

    private const val LINE_COUNTRY_START = 7
    private const val LINE_VALUE_START = 58
    private val AVAILABLE_VALUE_CHARS = arrayOf('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.', '-')

    private val logger = LogManager.getLogger(this)

    override val supportedCategories: List<Category>
        get() = CiaPathGetter.SUPPORTED_CATEGORIES

    override fun getMeasurements(category: Category) = NewThreadFinisher.sync {


        val path = CiaPathGetter.getPath(category)
        if (path == null) {
            logger.error("Category $category not supported by CiaMeasurementsReceiver")
            return@sync EMPTY_RESULT
        }

        val receivedBytes = SimpleLoader.load(path).getDataOrHandleError {
            logger.warn("Error while receiving category data from path $path: ${it.messageOrUndefined()}")
        } ?: return@sync EMPTY_RESULT

        val receivedString = tryCatch({
            String(receivedBytes, Charsets.UTF_8)
        }, {
            logger.warn("Error while converting to string data from path $path: ${it.messageOrUndefined()}")
        }) ?: return@sync EMPTY_RESULT

        val received = System.currentTimeMillis()
        return@sync receivedString.lines().mapNotNull { convertLineToMeasurement(it, category, received) }

    }

    private fun convertLineToMeasurement(line: String, category: Category, received: Long): Measurement? {
        if (line.isEmpty()) {
            return null
        }

        if (line.length < LINE_VALUE_START) {
            val message = "Unable to parse CIA line: '$line' to category: $category, its length < $LINE_VALUE_START"
            logger.warn(message)
            return null
        }

        val countryName = line.substring(LINE_COUNTRY_START, LINE_VALUE_START)

        val country = tryCatch({
            CiaCountryResolver.resolve(countryName)
        }, {
            logger.error("Unable to resolve country '$countryName'")
        }) ?: return null

        val stringValue = line.substring(LINE_VALUE_START).trim().filter { it in AVAILABLE_VALUE_CHARS }

        return tryOrElse({
            Measurement.create(
                    value = stringValue,
                    measured = null,
                    received = received,
                    country = country,
                    category = category
            )
        }, {
            logger.warn("Unable to convert string '$stringValue' to number for category: $category, country: $country")
            Measurement(
                    longValue = 0L,
                    floatValue = 0.0f,
                    country = country,
                    category = category,
                    received = received
            )
        })

    }
}