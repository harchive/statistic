package ru.hnau.statistic.data.update.cis

import ru.hnau.statistic_common.category.Category


object CiaPathGetter {

    private const val PATH_PREFIX = "https://www.cia.gov/library/publications/resources/the-world-factbook/rankorder/rawdata_"
    private const val PATH_SUFFIX = ".txt"

    private val PATHS_INFIXES = hashMapOf(
            Category.GDP_PURCHASING_POWER_PARITY to "2001",
            Category.POPULATION_GROWTH_RATE to "2002",
            Category.GDP_REAL_GROWTH_RATE to "2003",
            Category.GDP_PER_CAPITA_PPP to "2004",
            Category.MILITARY_EXPENDITURES to "2034",
            Category.AIRPORTS to "2053",
            Category.BIRTH_RATE to "2054",
            Category.DEATH_RATE to "2066",
            Category.EXPORTS to "2078",
            Category.DEBT_EXTERNAL to "2079",
            Category.ROADWAYS to "2085",
            Category.IMPORTS to "2087",
            Category.INDUSTRIAL_PRODUCTION_GROWTH_RATE to "2089",
            Category.INFANT_MORTALITY_RATE to "2091",
            Category.INFLATION_RATE_CONSUMER_PRICES to "2092",
            Category.WATERWAYS to "2093",
            Category.LABOR_FORCE to "2095",
            Category.LIFE_EXPECTANCY_AT_BIRTH to "2102",
            Category.MERCHANT_MARINE to "2108",
            Category.NET_MIGRATION_RATE to "2112",
            Category.POPULATION to "2119",
            Category.RAILWAYS to "2121",
            Category.TOTAL_FERTILITY_RATE to "2127",
            Category.UNEMPLOYMENT_RATE to "2129",
            Category.AREA to "2147",
            Category.TELEPHONES_MOBILE_CELLULAR to "2151",
            Category.INTERNET_USERS to "2153",
            Category.HIV_AIDS_ADULT_PREVALENCE_RATE to "2155",
            Category.HIV_AIDS_PEOPLE_LIVING_WITH_HIV_AIDS to "2156",
            Category.HIV_AIDS_DEATHS to "2157",
            Category.DISTRIBUTION_OF_FAMILY_INCOME_GINI_INDEX to "2172",
            Category.MEDIAN_AGE to "2177",
            Category.PUBLIC_DEBT to "2186",
            Category.RESERVES_OF_FOREIGN_EXCHANGE_AND_GOLD to "2188",
            Category.MARKET_VALUE_OF_PUBLICLY_TRADED_SHARES to "2200",
            Category.EDUCATION_EXPENDITURES to "2206",
            Category.CENTRAL_BANK_DISCOUNT_RATE to "2207",
            Category.COMMERCIAL_BANK_PRIME_LENDING_RATE to "2208",
            Category.STOCK_OF_DOMESTIC_CREDIT to "2211",
            Category.STOCK_OF_NARROW_MONEY to "2214",
            Category.STOCK_OF_BROAD_MONEY to "2215",
            Category.BUDGET_SURPLUS_OR_DEFICIT to "2222",
            Category.MATERNAL_MORTALITY_RATIO to "2223",
            Category.CHILDREN_UNDER_THE_AGE_OF_5_YEARS_UNDERWEIGHT to "2224",
            Category.HEALTH_EXPENDITURES to "2225",
            Category.OBESITY_ADULT_PREVALENCE_RATE to "2228",
            Category.UNEMPLOYMENT_YOUTH_AGES_15_24 to "2229",
            Category.ELECTRICITY_PRODUCTION to "2232",
            Category.ELECTRICITY_CONSUMPTION to "2233",
            Category.ELECTRICITY_FROM_FOSSIL_FUELS to "2237",
            Category.ELECTRICITY_FROM_HYDROELECTRIC_PLANTS to "2238",
            Category.ELECTRICITY_FROM_NUCLEAR_FUELS to "2239",
            Category.ELECTRICITY_FROM_OTHER_RENEWABLE_SOURCES to "2240",
            Category.CRUDE_OIL_PRODUCTION to "2241",
            Category.CRUDE_OIL_EXPORTS to "2242",
            Category.CRUDE_OIL_IMPORTS to "2243",
            Category.CRUDE_OIL_PROVED_RESERVES to "2244",
            Category.REFINED_PETROLEUM_PRODUCTS_PRODUCTION to "2245",
            Category.REFINED_PETROLEUM_PRODUCTS_CONSUMPTION to "2246",
            Category.REFINED_PETROLEUM_PRODUCTS_EXPORTS to "2247",
            Category.NATURAL_GAS_PRODUCTION to "2249",
            Category.NATURAL_GAS_CONSUMPTION to "2250",
            Category.NATURAL_GAS_EXPORTS to "2251",
            Category.NATURAL_GAS_PROVED_RESERVES to "2253",
            Category.CARBON_DIOXIDE_EMISSIONS_FROM_CONSUMPTION_OF_ENERGY to "2254",
            Category.GROSS_NATIONAL_SAVING to "2260"
    )

    val SUPPORTED_CATEGORIES = PATHS_INFIXES.keys.toList()

    fun getPath(category: Category): String? {
        val infix = PATHS_INFIXES[category] ?: return null
        return PATH_PREFIX + infix + PATH_SUFFIX
    }

}