package ru.hnau.statistic.data.entity

import org.springframework.data.mongodb.repository.MongoRepository
import ru.hnau.statistic_common.category.Category
import ru.hnau.statistic_common.country.Country


interface MeasurementDbRepository : MongoRepository<MeasurementDb, String> {

    fun findByCountry(country: Country): List<MeasurementDb>
    fun findByCategory(category: Category): List<MeasurementDb>

}