package ru.hnau.statistic.data.update.cis

import ru.hnau.statistic_common.country.Country


object CiaCountryResolver {

    private val SKIP_COUNTRIES = hashSetOf(
            "antarctica",
            "jan mayen", //Является частью Свальбард (Шпицберген) и Ян-Майен
            "gaza strip", // Сектор Газа.
            "dhekelia", "akrotiri", //Акроти́ри и Деке́лия (англ. Akrotiri and Dhekelia) — две британские военные базы на острове Кипр,
            "jersey", "guernsey", //Острова в проливе Ла-Манш
            "saint martin", "sint maarten", // Сен-Марте́н - остров в Карибском море в северной части гряды Восточных Карибских островов.
            "pitcairn islands", // Острова́ Пи́ткэрн — единственная заморская территория Великобритании в Тихом океане.
            "saint barthelemy",
            "united states pacific island wildlife refuges",
            "paracel islands",
            "wake island",
            "clipperton island",
            "navassa island",
            "spratly islands",
            "ashmore and cartier islands",
            "coral sea islands",
            "european union"
    )

    private val FIXED_COUNTRIES = hashMapOf(
            "united states" to Country.USA,
            "congo, democratic republic of the" to Country.CONGO_KINSHASA,
            "libya" to Country.LIBYAN_ARAB_JAMAHIRIYA_LIBYA,
            "burma" to Country.MYANMAR,
            "south sudan" to Country.SUDAN,
            "congo, republic of the" to Country.CONGO_BRAZZAVILLE,
            "western sahara" to Country.WEST_SAHARA,
            "korea, north" to Country.KOREA_REPUBLIC_OF,
            "korea, south" to Country.KOREA_DEMOCRATIC_PEOPLE_S_REPUBLIC_OF,
            "czechia" to Country.CZECH_REPUBLIC,
            "svalbard" to Country.SVALBARD_SPITSBERGEN_AND_JAN_MAYEN,
            "bhutan" to Country.BUTANE,
            "taiwan" to Country.TAIWAN_PROVINCE_OF_CHINA,
            "el salvador" to Country.SALVADOR,
            "timor-leste" to Country.EAST_TIMOR,
            "bahamas, the" to Country.BAHAMAS,
            "gambia, the" to Country.GAMBIA,
            "west bank" to Country.ISRAEL,
            "brunei" to Country.BRUNEI_DARUSSALAM,
            "cabo verde" to Country.CAPE_VERDE,
            "south georgia and south sandwich islands" to Country.SOUTH_GEORGIA_AND_THE_SOUTH_SANDWICH_ISLANDS,
            "micronesia, federated states of" to Country.MICRONESIA,
            "heard island and mcdonald islands" to Country.HEARD_AND_MCDONALD_ISLANDS,
            "saint helena, ascension, and tristan da cunha" to Country.ST_HELENA,
            "saint pierre and miquelon" to Country.SAINT_PIERRE_AND_MIQUELON,
            "american samoa" to Country.SAMOA,
            "british virgin islands" to Country.VIRGIN_ISLANDS_BRITISH,
            "french southern and antarctic lands" to Country.FRENCH_SOUTHERN_TERRITORIES,
            "macau" to Country.MACAU_MACAO,
            "holy see (vatican city)" to Country.VATICAN
    )

    fun resolve(name: String): Country? {
        val formattedName = name.trim().toLowerCase()
        if (formattedName in SKIP_COUNTRIES) {
            return null
        }
        return FIXED_COUNTRIES[formattedName]
                ?: Country.values().find { it.nameEnLower == formattedName }
                ?: throw RuntimeException("Undefined Country '$formattedName'")

    }

}