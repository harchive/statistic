package ru.hnau.statisticclient.pages.main.view

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Path
import android.graphics.RectF
import ru.hnau.androidutils.context_getters.DpPxGetter
import ru.hnau.androidutils.ui.utils.drawing.initAsRoundSideRect
import ru.hnau.androidutils.ui.utils.h_gravity.HGravity
import ru.hnau.androidutils.ui.view.label.Label
import ru.hnau.androidutils.ui.view.utils.setPadding
import ru.hnau.statisticclient.utils.ColorManager


class MainPageListItemInfoView(
        context: Context
) : Label(
        context = context,
        maxLines = 1,
        minLines = 1,
        textSize = DpPxGetter.fromDp(12),
        gravity = HGravity.CENTER,
        textColor = ColorManager.FG
) {

    companion object {

        private val PADDING_H = DpPxGetter.fromDp(12)
        private val PADDING_TOP = DpPxGetter.fromDp(10)
        private val PADDING_BOTTOM = DpPxGetter.fromDp(8)

    }

    private val memSaveRect = RectF()

    private val backgroundPath = Path()

    private val backgroundPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        color = ColorManager.BG_DARK.get(context)
    }

    init {
        setPadding(PADDING_H, PADDING_TOP, PADDING_H, PADDING_BOTTOM)
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)
        backgroundPath.initAsRoundSideRect(0f, 0f, width.toFloat(), height.toFloat(), memSaveRect)
    }

    override fun draw(canvas: Canvas) {
        canvas.drawPath(backgroundPath, backgroundPaint)
        super.draw(canvas)
    }

}