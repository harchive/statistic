package ru.hnau.statisticclient.pages.main.view

import android.annotation.SuppressLint
import android.content.Context
import ru.hnau.androidutils.ui.view.base_list.BaseList
import ru.hnau.statistic_common.category.Category
import ru.hnau.statistic_common.country.Country


@SuppressLint("ViewConstructor")
class MainPageList(
        context: Context,
        pageListType: MainPageListType,
        onCountrySelected: (Country) -> Unit,
        onCategorySelected: (Category) -> Unit
) : BaseList<MainPageListItem>(
        context = context,
        viewWrappersCreator = { MainPageListItemView(context) },
        itemsProducer = pageListType.getItemsProducer(onCountrySelected, onCategorySelected)
)