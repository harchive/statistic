package ru.hnau.statisticclient.utils

import ru.hnau.androidutils.context_getters.ColorGetter
import ru.hnau.androidutils.ui.drawables.waiter.WaiterDrawableColor
import ru.hnau.androidutils.ui.drawables.waiter.WaiterDrawableSize
import ru.hnau.androidutils.ui.ripple.RippleDrawInfo
import ru.hnau.androidutils.ui.view.buttons.CircleIconButtonShadowColor
import ru.hnau.androidutils.ui.view.header.HeaderInfo
import ru.hnau.androidutils.ui.view.header.back.button.HeaderBackButtonInfo
import ru.hnau.statisticclient.R


object ColorManager {

    val PRIMARY = ColorGetter.byResId(R.color.primary)
    val BG = ColorGetter.byResId(R.color.bg)
    val BG_DARK = ColorGetter.byResId(R.color.bg_dark)
    val FG = ColorGetter.byResId(R.color.fg)
    val FG_50 = ColorGetter.byResId(R.color.fg_50)
    val SELECT = ColorGetter.byResId(R.color.select)
    val SELECT_DARK = ColorGetter.byResId(R.color.select_dark)

    val DEFAULT_RIPPLE_DRAW_INFO = RippleDrawInfo(
            color = FG,
            backgroundColor = BG
    )

    val DEFAULT_HEADER_INFO = HeaderInfo(
            backgroundColor = BG
    )

    val DEFAULT_BACK_BUTTON_INFO = HeaderBackButtonInfo(
            color = FG
    )

    val DEFAULT_CIRCLE_BUTTON_SHADOW = CircleIconButtonShadowColor(
            alpha = 0.5f
    )

    val DEFAULT_CIRCLE_BUTTON_RIPPLE_DRAW_INFO = RippleDrawInfo(
            color = FG,
            backgroundColor = PRIMARY
    )

    val DEFAULT_WAITER_SIZE = WaiterDrawableSize.LARGE
    val DEFAULT_WAITER_COLOR = WaiterDrawableColor(
            foreground = PRIMARY,
            background = BG.mapWithAlpha(0.5f)
    )

}