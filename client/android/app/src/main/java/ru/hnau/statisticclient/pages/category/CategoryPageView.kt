package ru.hnau.statisticclient.pages.category

import android.annotation.SuppressLint
import android.content.Context
import android.view.Gravity
import android.widget.FrameLayout
import ru.hnau.androidutils.context_getters.DpPxGetter
import ru.hnau.androidutils.context_getters.DrawableGetter
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.ui.view.buttons.CircleIconButton
import ru.hnau.androidutils.ui.view.utils.*
import ru.hnau.statistic_common.category.Category
import ru.hnau.statistic_common.measurement.Measurement
import ru.hnau.statisticclient.R
import ru.hnau.statisticclient.api.CategoriesInfo
import ru.hnau.statisticclient.pages.views.MeasurementListItemView
import ru.hnau.statisticclient.pages.views.PageViewWithHeader
import ru.hnau.statisticclient.utils.ColorManager
import ru.hnau.statisticclient.view.list_loader.ListLoader


@SuppressLint("ViewConstructor")
class CategoryPageView(
        context: Context,
        category: Category,
        onShowOnMapButtonClicked: (context: Context, measurements: List<Measurement>) -> Unit,
        onBackButtonClicked: () -> Unit
) : PageViewWithHeader(
        context = context,
        onBackClicked = onBackButtonClicked,
        title = StringGetter(category.title),
        contentView = run {

            val listView = ListLoader(
                    context = context,
                    contentFinishersGetter = { CategoriesInfo[category] },
                    itemsViewsWrappersCreator = { MeasurementListItemView(context, { it.country.nameRu }) }
            )

            val mapButton = CircleIconButton(
                    context = context,
                    rippleDrawInfo = ColorManager.DEFAULT_CIRCLE_BUTTON_RIPPLE_DRAW_INFO,
                    onClick = { onShowOnMapButtonClicked.invoke(context, listView.itemsProducer.items) },
                    shadowColor = ColorManager.DEFAULT_CIRCLE_BUTTON_SHADOW,
                    icon = DrawableGetter(R.drawable.ic_open_on_map)
            ).apply {
                setFrameLayoutLayoutParams(WRAP_CONTENT, WRAP_CONTENT) {
                    gravity = Gravity.RIGHT or Gravity.BOTTOM
                    setRightPadding(DpPxGetter.fromDp(16))
                    setBottomPadding(DpPxGetter.fromDp(16))
                }
            }

            FrameLayout(context).apply {
                setLinearLayoutLayoutParams(MATCH_PARENT, 0, 1f)
                addView(listView)
                addView(mapButton)
            }

        }
)