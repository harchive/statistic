package ru.hnau.statisticclient.pages.map

import android.content.Context
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.ui.view.pager.Page
import ru.hnau.statistic_common.country.Country
import ru.hnau.statisticclient.view.map.MapViewCountryInfo


class MapPage(
        private val title: StringGetter,
        private val countriesInfo: Map<Country, MapViewCountryInfo>
) : Page<MapPageView>() {

    override fun generateView(context: Context) =
            MapPageView(
                    context = context,
                    title = title,
                    countriesInfo = countriesInfo,
                    onBackButtonClicked = this::onBackButtonClicked
            )

    private fun onBackButtonClicked() {
        goBack()
    }

}