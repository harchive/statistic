package ru.hnau.statisticclient.api

import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query
import ru.hnau.androidutils.utils.mapUi
import ru.hnau.jutils.finisher.NewThreadFinisher
import ru.hnau.jutils.possible.Possible
import ru.hnau.jutils.tryOrElse
import ru.hnau.jutils.tryOrNull
import ru.hnau.statistic_common.category.Category
import ru.hnau.statistic_common.country.Country
import ru.hnau.statistic_common.measurement.Measurement


interface Api {

    @GET("api/country-info")
    fun countryInfo(@Query("country") country: Country): Call<List<Measurement>>

    @GET("api/category-info")
    fun categoryInfo(@Query("category") category: Category): Call<List<Measurement>>

    companion object {

        fun countryInfo(country: Country) =
                callToFinisher(api.countryInfo(country))

        fun categoryInfo(category: Category) =
                callToFinisher(api.categoryInfo(category))

        private val api = run {

            val retrofit = Retrofit.Builder()
                    .baseUrl("http://hnau.ru:8080/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()

            return@run retrofit.create(Api::class.java)

        }

        private fun <T : Any> callToFinisher(call: Call<T>) =
                NewThreadFinisher.sync {
                    tryOrElse(
                            throwsAction = {
                                val result = call.execute()
                                if (!result.isSuccessful) {
                                    Possible.error<T>(result.errorBody().string())
                                } else {
                                    Possible.success(result.body())
                                }
                            },
                            onThrow = {
                                Possible.error(it)
                            }
                    )
                }.mapUi()


    }


}