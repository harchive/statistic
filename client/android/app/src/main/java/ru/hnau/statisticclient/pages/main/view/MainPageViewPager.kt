package ru.hnau.statisticclient.pages.main.view

import android.annotation.SuppressLint
import android.content.Context
import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import android.view.View
import android.view.ViewGroup
import ru.hnau.statistic_common.category.Category
import ru.hnau.statistic_common.country.Country


@SuppressLint("ViewConstructor")
class MainPageViewPager(
        context: Context,
        private val onCountrySelected: (Country) -> Unit,
        private val onCategorySelected: (Category) -> Unit
) : ViewPager(context) {

    private val items = MainPageListType.values()

    private val itemsAdapter = object : PagerAdapter() {

        override fun isViewFromObject(view: View, any: Any) = view == any

        override fun getCount() = items.size

        override fun instantiateItem(container: ViewGroup, position: Int): Any {
            val view = MainPageList(
                    context = context,
                    onCategorySelected = onCategorySelected,
                    onCountrySelected = onCountrySelected,
                    pageListType = items[position]
            )
            container.addView(view)
            return view
        }

        override fun destroyItem(container: ViewGroup, position: Int, any: Any) {
            (any as? View)?.let { container.removeView(it) }
        }

        override fun getPageTitle(position: Int) =
                items[position].title.get(context)

    }

    init {
        adapter = itemsAdapter
    }

}