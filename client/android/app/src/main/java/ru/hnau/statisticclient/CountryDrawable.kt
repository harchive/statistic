package ru.hnau.statisticclient

import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import ru.hnau.androidutils.ui.utils.drawing.doInState
import ru.hnau.statistic_common.country.Country
import ru.hnau.statistic_common.map.MapPoint
import ru.hnau.statisticclient.view.map.MapDrawable


class CountryDrawable(
        country: Country
) : MapDrawable() {

    companion object {

        private const val BASE_TEXT_SIZE = 100f

    }

    private val textPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        color = Color.BLACK
        textAlign = Paint.Align.CENTER
        textSize = BASE_TEXT_SIZE
    }

    private val name = country.nameRu

    init {

        textPaint.textSize = BASE_TEXT_SIZE * MapDrawable.BASE_SCALE / textPaint.measureText(name)

    }

    override fun draw(canvas: Canvas) {
        canvas.drawText(name, 0f, 0f + textPaint.textSize / 2, textPaint)
    }

}