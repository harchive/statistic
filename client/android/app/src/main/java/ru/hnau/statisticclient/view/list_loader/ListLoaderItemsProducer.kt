package ru.hnau.statisticclient.view.list_loader

import ru.hnau.jutils.producer.DataProducer


class ListLoaderItemsProducer<T : Any> : DataProducer<List<T>>(emptyList()) {

    var items: List<T>
        set(value) {
            data = value
        }
        get() = data

}