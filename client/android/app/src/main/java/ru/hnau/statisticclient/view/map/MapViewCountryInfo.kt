package ru.hnau.statisticclient.view.map

import ru.hnau.androidutils.context_getters.ColorGetter


data class MapViewCountryInfo(
        val color: ColorGetter? = null,
        val drawable: MapDrawable? = null
)