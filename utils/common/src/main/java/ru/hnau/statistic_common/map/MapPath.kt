package ru.hnau.statistic_common.map

data class MapPath(
        val points: List<MapPoint>,
        val outerRect: MapRect?
)