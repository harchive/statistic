package ru.hnau.statistic_common.map


data class MapRect(
        val left: Float,
        val top: Float,
        val right: Float,
        val bottom: Float
)