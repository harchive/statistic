package ru.hnau.statistic_common.map

import kotlin.math.sqrt

data class MapPoint(
        val x: Float,
        val y: Float
) {

    operator fun plus(other: MapPoint) =
            MapPoint(
                    this.x + other.x,
                    this.y + other.y
            )

    operator fun minus(other: MapPoint) =
            MapPoint(
                    this.x - other.x,
                    this.y - other.y
            )

    operator fun div(number: Float) =
            MapPoint(
                    x / number,
                    y / number
            )

    fun distanceTo(other: MapPoint): Float {
        val dx = x - other.x
        val dy = y - other.y
        return sqrt(dx * dx + dy * dy)
    }

}