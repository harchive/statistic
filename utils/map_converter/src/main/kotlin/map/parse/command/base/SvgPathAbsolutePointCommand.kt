package map.parse.command.base

import ru.hnau.statistic_common.map.MapPoint


abstract class SvgPathAbsolutePointCommand(
        p: MapPoint,
        line: Boolean
) : SvgPathPointCommand(
        p,
        line
) {

    override fun calcNewPos(oldPos: MapPoint) = p

}