package map.parse.command.base

import ru.hnau.statistic_common.map.MapPoint


abstract class SvgPathPointCommand(
        val p: MapPoint,
        line: Boolean
) : SvgPathCommand(
        line
) {

    override fun getParams() = listOf(p.x, p.y)

}