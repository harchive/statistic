package map.parse.command


import map.parse.command.base.SvgPathCommand
import ru.hnau.statistic_common.map.MapPoint


class SvgPathCloseCommand : SvgPathCommand(true) {

    override fun getParams() = emptyList<Float>()

    override fun calcNewPos(oldPos: MapPoint): MapPoint? = null

}