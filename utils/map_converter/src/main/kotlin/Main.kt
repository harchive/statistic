import com.google.gson.Gson
import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.runBlocking
import map.parse.*
import ru.hnau.statistic_common.country.Country
import ru.hnau.statistic_common.map.*
import svg_converter.SvgMapConverter
import java.io.PrintWriter
import kotlin.math.sqrt

object Main {

    private const val SEARCH_CENTER_CIRCLE_STEP = 1f

    @JvmStatic
    fun main(args: Array<String>) {

        val countriesWithSvgPaths =
                SvgMapConverter.convertFileToCountriesWithSvgPaths("world.svg")

        val countriesWithPathsRaw =
                countriesWithSvgPaths.mapValues { (_, svgPath) -> svgPathToMapPaths(svgPath) }

        val (countriesWithPaths, size) =
                normalizeCountriesWithPaths(countriesWithPathsRaw)

        val countriesWithPathsWithOuterRects =
                addOuterRectsToCountriesPaths(countriesWithPaths)

        val countriesWithPathsWithCircles =
                countriesWithPathsToCountriesWithPathsWithCenterCircle(countriesWithPathsWithOuterRects, SEARCH_CENTER_CIRCLE_STEP)

        val result = CountriesMap(
                size,
                countriesWithPathsWithCircles.mapValues { (_, paths) ->
                    MapPaths(
                            paths.first.map {
                                MapPath(
                                        it.first,
                                        it.second
                                )
                            },
                            paths.second
                    )
                }
        )


        val file = PrintWriter("CountriesMapInfo.json")
        file.println(Gson().toJson(result))
        file.close()

    }

    private fun svgPathToMapPaths(svgPath: String) =
            ParseCollector.collect(
                    SvgPartCommandsAggregator(
                            SvgPathSymbolsAggregator(
                                    SvgPartCharsAggregator(
                                            ParseProducer(svgPath)
                                    )
                            )
                    )
            )

    private fun normalizeCountriesWithPaths(
            countriesWithPaths: Map<Country, List<List<MapPoint>>>
    ): Pair<Map<Country, List<List<MapPoint>>>, MapPoint> {

        var left = Float.MAX_VALUE
        var top = Float.MAX_VALUE
        var right = Float.MIN_VALUE
        var bottom = Float.MIN_VALUE

        countriesWithPaths.values.forEach {
            it.forEach {
                it.forEach { (x, y) ->

                    if (x < left) {
                        left = x
                    } else {
                        if (x > right) {
                            right = x
                        }
                    }

                    if (y < top) {
                        top = y
                    } else {
                        if (y > bottom) {
                            bottom = y
                        }
                    }

                }
            }
        }

        val size = MapPoint(right - left, bottom - top)
        val offset = MapPoint(-left, -top)

        val newMap = countriesWithPaths.mapValues { (_, paths) ->
            paths.map { it.map { it + offset } }
        }

        return newMap to size

    }

    private fun addOuterRectsToCountriesPaths(
            countriesPaths: Map<Country, List<List<MapPoint>>>
    ) =
            countriesPaths.mapValues { (_, paths) ->
                paths.map {
                    it to calcOuterRectForPath(it)
                }
            }

    private fun countriesWithPathsToCountriesWithPathsWithCenterCircle(
            countriesWithPaths: Map<Country, List<Pair<List<MapPoint>, MapRect?>>>,
            step: Float
    ): Map<Country, Pair<List<Pair<List<MapPoint>, MapRect?>>, MapCircle?>> {

        val convertingStart = System.currentTimeMillis()

        val result = runBlocking {

            countriesWithPaths
                    .mapValues { (country, paths) ->
                        async { paths to getCenterCircleOfPaths(country, paths, step) }
                    }
                    .mapValues { (_, paths) ->
                        paths.await()
                    }

        }

        val convertingEnd = System.currentTimeMillis()
        val convertingTime = convertingEnd - convertingStart
        println("Converting time: ${timeToString(convertingTime)}")

        return result
    }


    private fun getCenterCircleOfPaths(country: Country, paths: List<Pair<List<MapPoint>, MapRect?>>, step: Float): MapCircle? {

        if (country == Country.RUSSIA) {
            println("")
        }

        val countryStart = System.currentTimeMillis()

        val path = paths.maxBy { it.first.size }
        val result = path?.let { getCenterCircleOfPath(it.first, it.second, step) }

        val countryEnd = System.currentTimeMillis()
        val countryTime = countryEnd - countryStart
        println("${country.nameRu}: ${timeToString(countryTime)}")

        return result
    }

    private fun getCenterCircleOfPath(path: List<MapPoint>, outRect: MapRect?, step: Float): MapCircle? {
        outRect ?: return null

        var result: MapCircle? = null

        var x = outRect.left
        while (x < outRect.right) {

            var y = outRect.top
            while (y < outRect.bottom) {

                val radius = getCenterCircleRadiusForPoint(x, y, path)
                val localResult = result
                if (radius != null && (localResult == null || localResult.radius < radius)) {
                    result = MapCircle(MapPoint(x, y), radius)
                }

                y += step
            }

            x += step
        }

        return result
    }

    private fun getCenterCircleRadiusForPoint(x: Float, y: Float, path: List<MapPoint>): Float? {
        if (!pathContainsPoint(x, y, path)) {
            return null
        }
        return getMinDistanceToPath(x, y, path)
    }

    private fun getMinDistanceToPath(x: Float, y: Float, path: List<MapPoint>): Float {
        var distance = Float.MAX_VALUE
        path.forEach {
            val distanceToPoint = distanceToMapPoint(x, y, it)
            if (distanceToPoint < distance) {
                distance = distanceToPoint
            }
        }
        return distance
    }

    private fun distanceToMapPoint(x: Float, y: Float, point: MapPoint) =
            sqrt((x - point.x) * (x - point.x) + (y - point.y) * (y - point.y))

    private fun pathContainsPoint(x: Float, y: Float, path: List<MapPoint>): Boolean {
        if (path.size < 3) {
            return false
        }
        var result = false
        var j = path.size - 1
        for (i in 0 until path.size) {
            val iX = path[i].x
            val iY = path[i].y
            val jX = path[j].x
            val jY = path[j].y
            if ((iY < y && jY >= y || jY < y && iY >= y) && (iX + (y - iY) / (jY - iY) * (jX - iX) < x)) {
                result = !result
            }
            j = i
        }
        return result
    }

    private fun calcOuterRectForPath(path: List<MapPoint>): MapRect? {

        if (path.size < 3) {
            return null
        }

        var left = Float.MAX_VALUE
        var top = Float.MAX_VALUE
        var right = Float.MIN_VALUE
        var bottom = Float.MIN_VALUE

        path.forEach {

            val x = it.x
            if (x < left) {
                left = x
            } else {
                if (x > right) {
                    right = x
                }
            }

            val y = it.y
            if (y < top) {
                top = y
            } else {
                if (y > bottom) {
                    bottom = y
                }
            }

        }

        if (left >= right || top >= bottom) {
            return null
        }
        return MapRect(left, top, right, bottom)

    }

    private fun timeToString(time: Long): String {
        var ms = time
        var s = ms / 1000
        ms -= s * 1000
        var m = s / 60
        s -= m * 60
        val h = m / 60
        m -= h * 60

        return longToStr(h, 2) + ":" + longToStr(m, 2) + ":" + longToStr(s, 2) + ":" + longToStr(ms, 3)
    }

    private fun longToStr(long: Long, digits: Int): String {
        val result = long.toString()
        if (result.length >= digits) {
            return result
        }
        return (0 until digits - result.length).joinToString("") { "0" } + result
    }

}